module.exports = {
  filenameHashing: false,
  pages: {
    index: {
      entry: "./src/main.js",
      template: "public/home.html",
      filename: 'home.index',
      title: "Siffi App",
      chunks: ["chunk-vendors", "chunk-common", "index"]
    },
    faq: {
      entry: "./src/faq.js",
      template: 'public/faq.html',
      filename: 'faq.html',
      title: 'FAQ',
      chunks: [ 'chunk-vendors', 'chunk-common', 'index' ]
    },
  }
};
