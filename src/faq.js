import Vue from "vue";
import Faq from "./Faq.vue";
import router from "./router";

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(Faq)
}).$mount("#faq");
