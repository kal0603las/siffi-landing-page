import Vue from "vue";
import VueRouter from "vue-router";
import Home from "/src/views/Home.vue";
import Faq from "/src/Faq.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "faq",
    name: "Faq",
    component: Faq
  }
];

const router = new VueRouter({
  routes
});

export default router;
